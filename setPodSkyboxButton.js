/*global Entities, print*/

(function () {
    var keys = Object.keys(this);
    print("Enabling click handler!");
    this.mousePressOnEntity = function (entityID, event) {
        print("I was clicked: " + entityID);
        var entityProps = Entities.getEntityProperties(entityID, ["userData"]);
        var userData = JSON.parse(entityProps.userData);
        print("entityProps: " + Object.keys(entityProps).join(", "));
        print("userData: ", userData);
        print("userData: " + Object.keys(userData).join(", "));
        if (userData.skyboxId) {
            var skyboxProps = Entities.getEntityProperties(userData.skyboxId);
            if (!skyboxProps) {
                print("skybox was not found!");
            }
            if (userData.skyboxImage) {
                print("Setting the skybox image");
                Entities.editEntity(userData.skyboxId, {
                    visible: true,
                    skybox: {
                        url: userData.skyboxImage
                    }
                });
            } else {
                print("Clearing the skybox image");
                Entities.editEntity(userData.skyboxId, {
                    visible: false
                });
            }
        } else {
            print("Skybox was undefined");
        }
    };
})

