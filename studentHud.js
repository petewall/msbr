/*global AnimationCache, Controller, Entities, MyAvatar, Overlays, print, Script, Vec3 */

(function () {

    var windowWidth = 0,
        windowHeight = 0,

        background2D = {},
        attentionButton2D = {},
        yesButton2D = {},
        noButton2D = {},
        laughButton2D = {},
        blueButton2D = {},
        redButton2D = {},
        greenButton2D = {},
        yellowButton2D = {},

        yesAnimationURL = "http://petewall.net/files/kevguitaranimation.fbx",
        noAnimationURL = "http://petewall.net/files/kevguitaranimation.fbx",
        laughAnimationURL = "http://petewall.net/files/kevguitaranimation.fbx",

        seatingAreaBox = "{74888c07-727c-4fc3-95bc-58590b73298c}";

    function createOverlays() {
        background2D.overlay = Overlays.addOverlay("rectangle", {
            color: { red: 255, green: 255, blue: 255 },
            width: background2D.width,
            height: background2D.height,
            visible: true,
            alpha: 1.0
        });
        attentionButton2D.overlay = Overlays.addOverlay("text", {
            color: { red: 255, green: 0, blue: 0 },
            text: "Attention!",
            width: attentionButton2D.width,
            height: attentionButton2D.height,
            visible: true,
            alpha: 1.0
        });
        yesButton2D.overlay = Overlays.addOverlay("text", {
            color: { red: 255, green: 255, blue: 255 },
            text: "Yes",
            width: yesButton2D.width,
            height: yesButton2D.height,
            visible: true,
            alpha: 1.0
        });
        noButton2D.overlay = Overlays.addOverlay("text", {
            color: { red: 255, green: 255, blue: 255 },
            text: "No",
            width: noButton2D.width,
            height: noButton2D.height,
            visible: true,
            alpha: 1.0
        });
        laughButton2D.overlay = Overlays.addOverlay("text", {
            color: { red: 255, green: 255, blue: 255 },
            text: "Laugh",
            width: laughButton2D.width,
            height: laughButton2D.height,
            visible: true,
            alpha: 1.0
        });
        blueButton2D.overlay = Overlays.addOverlay("rectangle", {
            color: { red: 0, green: 0, blue: 255 },
            width: blueButton2D.width,
            height: blueButton2D.height,
            visible: true,
            alpha: 1.0
        });
        redButton2D.overlay = Overlays.addOverlay("rectangle", {
            color: { red: 255, green: 0, blue: 0},
            width: redButton2D.width,
            height: redButton2D.height,
            visible: true,
            alpha: 1.0
        });
        greenButton2D.overlay = Overlays.addOverlay("rectangle", {
            color: { red: 0, green: 255, blue: 0 },
            width: greenButton2D.width,
            height: greenButton2D.height,
            visible: true,
            alpha: 1.0
        });
        yellowButton2D.overlay = Overlays.addOverlay("rectangle", {
            color: { red: 255, green: 255, blue: 0},
            width: yellowButton2D.width,
            height: yellowButton2D.height,
            visible: true,
            alpha: 1.0
        });
    }

    function deleteOverlays() {
        Overlays.deleteOverlay(background2D.overlay);
        Overlays.deleteOverlay(attentionButton2D.overlay);
        Overlays.deleteOverlay(yesButton2D.overlay);
        Overlays.deleteOverlay(noButton2D.overlay);
        Overlays.deleteOverlay(laughButton2D.overlay);
        Overlays.deleteOverlay(blueButton2D.overlay);
        Overlays.deleteOverlay(redButton2D.overlay);
        Overlays.deleteOverlay(greenButton2D.overlay);
        Overlays.deleteOverlay(yellowButton2D.overlay);
    }

    function update() {
        var viewport = Controller.getViewportDimensions();

        if (viewport.x !== windowWidth || viewport.y !== windowHeight) {
            print("Running update");
            windowWidth = viewport.x;
            windowHeight = viewport.y;

            Overlays.editOverlay(background2D.overlay, {
                x: 0,
                y: windowHeight - background2D.height
            });
            Overlays.editOverlay(attentionButton2D.overlay, {
                x: (background2D.width - attentionButton2D.width) / 2,
                y: windowHeight - background2D.height + 10
            });
            Overlays.editOverlay(yesButton2D.overlay, {
                x: 8,
                y: windowHeight - background2D.height + 61
            });
            Overlays.editOverlay(noButton2D.overlay, {
                x: 60,
                y: windowHeight - background2D.height + 61
            });
            Overlays.editOverlay(laughButton2D.overlay, {
                x: 113,
                y: windowHeight - background2D.height + 61
            });
            Overlays.editOverlay(blueButton2D.overlay, {
                x: 10,
                y: windowHeight - background2D.height + 112
            });
            Overlays.editOverlay(redButton2D.overlay, {
                x: 47,
                y: windowHeight - background2D.height + 112
            });
            Overlays.editOverlay(greenButton2D.overlay, {
                x: 87,
                y: windowHeight - background2D.height + 112
            });
            Overlays.editOverlay(yellowButton2D.overlay, {
                x: 127,
                y: windowHeight - background2D.height + 112
            });
        }
    }

    function vec2s(v) {
        if (v) {
            return "<" + v.x + ", " + v.y + ", " + v.z + ">";
        }
        return "<?, ?, ?>";
    }

    function centerToCorner(position, dimensions) {
        var halfDimensions = Vec3.multiply(dimensions, 0.5),
            corner = Vec3.subtract(position, halfDimensions);
        return corner;
    }

    function findClosestSeat() {
        print("Finding the closest seat!");
        var seatingArea = Entities.getEntityProperties(seatingAreaBox, ["position", "dimensions"]),
            corner = centerToCorner(seatingArea.position, seatingArea.dimensions),
            entities = Entities.findEntitiesInBox(corner, seatingArea.dimensions),
            closestDistance,
            closestId,
            i,
            props,
            distance;
        for (i = 0; i < entities.length; i += 1) {
            props = Entities.getEntityProperties(entities[i], ["name", "position"]);
            distance = Vec3.distance(MyAvatar.position, props.position);
            if (props.name === "Seat") {
                if (!closestDistance || distance < closestDistance) {
                    closestDistance = distance;
                    closestId = entities[i];
                }
            }
        }

        print("returning " + closestId);
        return closestId;
    }

    function onMousePressEvent(event) {
        var seatId;
            clickedOverlay = Overlays.getOverlayAtPoint({
                x: event.x,
                y: event.y
            });

        if (attentionButton2D.overlay === clickedOverlay) {
            Overlays.editOverlay(attentionButton2D.overlay, {
                backgroundColor: { red: 255, green: 255, blue: 255 }
            });
        } else if (yesButton2D.overlay === clickedOverlay) {
            print("Playing yes animation");
            MyAvatar.overrideAnimation(yesAnimationURL, 30, false, 0, 45);
            Script.setTimeout(function () {
                print("resetting animaiton");
                MyAvatar.restoreAnimation();
            }, 1500);
        } else if (noButton2D.overlay === clickedOverlay) {
            print("Playing no animation");
            MyAvatar.overrideAnimation(noAnimationURL, 30, false, 0, 45);
            Script.setTimeout(function () {
                print("resetting animaiton");
                MyAvatar.restoreAnimation();
            }, 1500);
        } else if (laughButton2D.overlay === clickedOverlay) {
            print("Playing laugh animation");
            MyAvatar.overrideAnimation(laughAnimationURL, 30, false, 0, 45);
            Script.setTimeout(function () {
                print("resetting animaiton");
                MyAvatar.restoreAnimation();
            }, 1500);
        } else if (blueButton2D.overlay === clickedOverlay) {
            seatId = findClosestSeat();
            if (seatId) {
                Entities.editEntity(seatId, {"color": {"red": 0, "green": 0, "blue": 255}});
            }
        } else if (redButton2D.overlay === clickedOverlay) {
            seatId = findClosestSeat();
            if (seatId) {
                Entities.editEntity(seatId, {"color": {"red": 255, "green": 0, "blue": 0}});
            }
        } else if (greenButton2D.overlay === clickedOverlay) {
            seatId = findClosestSeat();
            if (seatId) {
                print("Setting color to green");
                Entities.editEntity(seatId, {"color": {"red": 0, "green": 255, "blue": 0}});
            }
        } else if (yellowButton2D.overlay === clickedOverlay) {
            seatId = findClosestSeat();
            if (seatId) {
                Entities.editEntity(seatId, {"color": {"red": 255, "green": 255, "blue": 0}});
            }
        }
    }

    function setUp() {
        print("Running setup");
        background2D.height = 150;
        background2D.width = 165;

        attentionButton2D.height = 44;
        attentionButton2D.width = 150;

        yesButton2D.height = noButton2D.height = laughButton2D.height = 44;
        yesButton2D.width = noButton2D.width= laughButton2D.width = 44;

        blueButton2D.height = redButton2D.height = greenButton2D.height = yellowButton2D.height = 30;
        blueButton2D.width = redButton2D.width = greenButton2D.width = yellowButton2D.width = 30;

        createOverlays();
        Controller.mousePressEvent.connect(onMousePressEvent);

        AnimationCache.prefetch(yesAnimationURL);
        AnimationCache.prefetch(noAnimationURL);
        AnimationCache.prefetch(laughAnimationURL);
    }

    function tearDown() {
        deleteOverlays();
    }

    setUp();
    Script.update.connect(update);
    Script.scriptEnding.connect(tearDown);
}());
