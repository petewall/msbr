/*global Entities, print*/

(function () {
    print("Enabling click handler!");
    var sound = SoundCache.getSound("http://localhost:8080/game_over32.wav");
    this.mousePressOnEntity = function (entityID, event) {
        print("I was clicked: " + entityID);
        if (sound.downloaded) {
            print("playing sound: ", sound);
            Audio.playSound(sound);
        }
    };
})

