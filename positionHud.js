/*global Controller, MyAvatar, Overlays, Script */

(function () {

    var background = {},
        xPos = {},
        yPos = {},
        zPos = {};


    function createOverlays() {
        background.overlay = Overlays.addOverlay("rectangle", {
            color: { red: 255, green: 255, blue: 255 },
            width: background.width,
            height: background.height,
            visible: true,
            alpha: 1.0
        });
        xPos.overlay = Overlays.addOverlay("text", {
            color: { red: 0, green: 0, blue: 0 },
            text: "X: ---",
            width: xPos.width,
            height: xPos.height,
            visible: true,
            alpha: 1.0
        });
        yPos.overlay = Overlays.addOverlay("text", {
            color: { red: 0, green: 0, blue: 0 },
            text: "Y: ---",
            width: xPos.width,
            height: xPos.height,
            visible: true,
            alpha: 1.0
        });
        zPos.overlay = Overlays.addOverlay("text", {
            color: { red: 0, green: 0, blue: 0 },
            text: "Z: ---",
            width: xPos.width,
            height: xPos.height,
            visible: true,
            alpha: 1.0
        });
    }

    function deleteOverlays() {
        Overlays.deleteOverlay(background.overlay);
        Overlays.deleteOverlay(xPos.overlay);
        Overlays.deleteOverlay(yPos.overlay);
        Overlays.deleteOverlay(zPos.overlay);
    }

    function update() {
        var viewport = Controller.getViewportDimensions(),
            pos = MyAvatar.position;
        Overlays.editOverlay(background.overlay, {
            x: viewport.x - background.width,
            y: 0
        });
        Overlays.editOverlay(xPos.overlay, {
            x: viewport.x - xPos.width - 5,
            y: 5,
            text: "X: " + pos.x
        });
        Overlays.editOverlay(yPos.overlay, {
            x: viewport.x - yPos.width - 5,
            y: 5 + xPos.height,
            text: "Y: " + pos.y
        });
        Overlays.editOverlay(zPos.overlay, {
            x: viewport.x - zPos.width - 5,
            y: 5 + xPos.height + yPos.height,
            text: "Z: " + pos.z
        });
    }

    function setUp() {
        background.height = 70;
        background.width = 110;

        xPos.height = yPos.height = zPos.height = 20;
        xPos.width = yPos.width = zPos.width = 100;

        createOverlays();
    }

    function tearDown() {
        deleteOverlays();
    }

    setUp();
    Script.update.connect(update);
    Script.scriptEnding.connect(tearDown);
}());
